package bot

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	getUpdatesSingleMessageWithURLData = "testdata/getupdates_messages_only.json"
	getUpdatesListCommandData          = "testdata/getupdates_command_list.json"
)

func TestNewTgBot(t *testing.T) {
	a := assert.New(t)

	bot, err := NewTgBot("test-token", newTestClient(t, getUpdatesSingleMessageWithURLData))

	a.NoError(err)
	a.Implements((*Bot)(nil), bot)
}

func TestTgBot_ListenUpdates(t *testing.T) {
	testCases := []struct {
		name                    string
		updatesResponseDataPath string
		expectedResult          *MessageUpdate
	}{
		{
			name:                    "Message with single URL",
			updatesResponseDataPath: getUpdatesSingleMessageWithURLData,
			expectedResult: &MessageUpdate{
				ChatID:      123,
				MessageID:   1,
				Text:        "текст text http://example.org",
				URLEntities: []string{"http://example.org"},
			},
		},
		{
			name:                    "/list command",
			updatesResponseDataPath: getUpdatesListCommandData,
			expectedResult: &MessageUpdate{
				ChatID:      123,
				MessageID:   1,
				Text:        "/list",
				IsCommand:   true,
				Command:     "list",
				URLEntities: make([]string, 0),
			},
		},
	}

	for _, tt := range testCases {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			a := assert.New(t)

			bot, err := NewTgBot("test-token", newTestClient(t, tt.updatesResponseDataPath))
			a.NoError(err)

			for u := range bot.ListenMessages() {
				a.Equal(tt.expectedResult, u)
				bot.StopListeningUpdates()
			}
		})
	}
}

func newTestClient(t *testing.T, getUpdatesResponseDataPath string) *http.Client {
	return &http.Client{
		Transport: &tgTransport{
			test:                       t,
			getUpdatesResponseDataPath: getUpdatesResponseDataPath,
		},
	}
}

type tgTransport struct {
	test                       *testing.T
	getUpdatesResponseDataPath string
}

func (t *tgTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	urlPath := strings.ToLower(req.URL.Path)
	if !strings.HasPrefix(urlPath, "/bottest-token") {
		t.test.Fatal("token missing")
	}

	if strings.HasSuffix(urlPath, "/getme") {
		return t.getGetMeResponse(req)
	}

	if strings.HasSuffix(urlPath, "/getupdates") {
		return t.getGetUpdatesResponse(req)
	}

	return &http.Response{}, fmt.Errorf("unexpected request")
}

func (t *tgTransport) getGetMeResponse(req *http.Request) (*http.Response, error) {
	fd, err := os.OpenFile("testdata/getme.json", os.O_RDONLY, 0644)
	if err != nil {
		t.test.Fatal(err)
	}

	stat, err := fd.Stat()
	if err != nil {
		t.test.Fatal(err)
	}

	return &http.Response{
		StatusCode:    200,
		Body:          fd,
		ContentLength: stat.Size(),
		Request:       req,
	}, nil
}

func (t *tgTransport) getGetUpdatesResponse(req *http.Request) (*http.Response, error) {
	fd, err := os.OpenFile(t.getUpdatesResponseDataPath, os.O_RDONLY, 0644)
	if err != nil {
		t.test.Fatal(err)
	}

	stat, err := fd.Stat()
	if err != nil {
		t.test.Fatal(err)
	}

	return &http.Response{
		StatusCode:    200,
		Body:          fd,
		ContentLength: stat.Size(),
		Request:       req,
	}, nil
}
