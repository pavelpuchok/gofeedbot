package bot

type MessagesChannel <-chan *MessageUpdate

type Bot interface {
	ListenMessages() MessagesChannel
	Send(chatID int64, text string) error
	SendReply(chatID int64, messageID int, text string) error
	StopListeningUpdates()
}
