package bot

import (
	"net/http"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type TgBot struct {
	api                  *tgbotapi.BotAPI
	stopListeningChannel chan bool
}

type Token string

func NewTgBot(token Token, client *http.Client) (*TgBot, error) {
	api, err := tgbotapi.NewBotAPIWithClient(string(token), client)
	if err != nil {
		return nil, err
	}

	return &TgBot{api: api, stopListeningChannel: make(chan bool, 0)}, nil
}

func (b *TgBot) ListenMessages() MessagesChannel {
	c := make(chan *MessageUpdate, 100)

	go func() {
		u := tgbotapi.NewUpdate(0)
		u.Timeout = 10

		updates, _ := b.api.GetUpdatesChan(u)

		for {
			select {
			case <-b.stopListeningChannel:
				b.api.StopReceivingUpdates()
				close(c)
				return
			case update := <-updates:
				if update.Message != nil {
					c <- newMessageUpdate(update.Message)
				}
			}
		}
	}()

	return c
}

func (b *TgBot) Send(chatID int64, text string) error {
	msg := tgbotapi.NewMessage(chatID, text)
	msg.ParseMode = tgbotapi.ModeMarkdown
	_, err := b.api.Send(msg)
	return err
}

func (b *TgBot) SendReply(chatID int64, messageID int, text string) error {
	msg := tgbotapi.NewMessage(chatID, text)
	msg.ReplyToMessageID = messageID
	msg.ParseMode = tgbotapi.ModeMarkdown
	_, err := b.api.Send(msg)
	return err
}

func (b *TgBot) StopListeningUpdates() {
	close(b.stopListeningChannel)
}
