package bot

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

type MessageUpdate struct {
	ChatID      int64
	MessageID   int
	Text        string
	URLEntities []string
	IsCommand   bool
	Command     string
}

func newMessageUpdate(message *tgbotapi.Message) *MessageUpdate {
	return &MessageUpdate{
		ChatID:      message.Chat.ID,
		MessageID:   message.MessageID,
		Text:        message.Text,
		URLEntities: extractURLEntities(message.Entities, message.Text),
		IsCommand:   message.IsCommand(),
		Command:     message.Command(),
	}
}

func extractURLEntities(entities *[]tgbotapi.MessageEntity, messageText string) []string {
	result := []string{}

	if entities != nil {
		for _, e := range *entities {
			switch e.Type {
			case "text_link":
				result = append(result, e.URL)
				break
			case "url":
				runes := []rune(messageText)[e.Offset : e.Offset+e.Length]
				result = append(result, string(runes))
			}
		}
	}

	return result
}
