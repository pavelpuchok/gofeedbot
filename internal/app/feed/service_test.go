package feed

import (
	"fmt"
	"gofeedbot/internal/app/log"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var (
	time12h    = time.Unix(1577880000, 0) // 2020-01-01T12:00:00+00:00
	time12h30m = time.Unix(1577881800, 0) // 2020-01-01T12:30:00+00:00
	time13h    = time.Unix(1577883600, 0) // 2020-01-01T13:00:00+00:00
)

func TestService_SubscribeRSSFeedCase(t *testing.T) {
	service := givenService(t)
	feed, err := whenSubscribeCalledWithRSSFeed(t, service)
	thenRSSFeedShouldBeParsedAndSavedCorrectly(t, feed, err)
}

func givenService(t *testing.T) *Service {
	t.Helper()

	db, err := NewDB(":memory:")
	if err != nil {
		t.Fatal(err)
	}

	store, err := NewSQLiteStore(db, log.New(log.FatalLevel))
	if err != nil {
		t.Fatal(err)
	}

	t.Cleanup(func() {
		db.Close()
	})

	return New(store, ParseRSS, NewDefaultFetcher())
}

func whenSubscribeCalledWithRSSFeed(t *testing.T, service *Service) (*Feed, error) {
	t.Helper()

	url := runTestFeedServer(t)

	return service.Subscribe(1, url+"/rss")
}

func thenRSSFeedShouldBeParsedAndSavedCorrectly(t *testing.T, feed *Feed, err error) {
	t.Helper()
	a := assert.New(t)

	expectedFeed := getTestFeed()

	a.NoError(err)
	a.Equal(expectedFeed, feed)
}

func TestService_SubscribeAtomFeedCase(t *testing.T) {
	service := givenService(t)
	feed, err := whenSubscribeCalledWithAtomFeed(t, service)
	thenAtomFeedShouldBeParsedAndSavedCorrectly(t, feed, err)
}

func whenSubscribeCalledWithAtomFeed(t *testing.T, service *Service) (*Feed, error) {
	t.Helper()

	url := runTestFeedServer(t)

	return service.Subscribe(1, url+"/atom")
}

func thenAtomFeedShouldBeParsedAndSavedCorrectly(t *testing.T, feed *Feed, err error) {
	t.Helper()
	a := assert.New(t)

	expectedFeed := getTestFeed()

	a.NoError(err)
	a.Equal(expectedFeed, feed)
}

func TestService_SubscribeFeedWithoutSelfLinkCase(t *testing.T) {
	service, url := givenServiceAndFeedServerWithoutSelfLink(t)
	feed, err := whenSubscribeCalledWithFeedWithoutSelfLink(t, service, url)
	thenSubscribeShouldBeSavedCorrectly(t, feed, err, url)
}

func givenServiceAndFeedServerWithoutSelfLink(t *testing.T) (*Service, string) {
	t.Helper()

	service := givenService(t)
	url := runTestFeedServer(t)

	return service, url
}

func whenSubscribeCalledWithFeedWithoutSelfLink(t *testing.T, service *Service, url string) (*Feed, error) {
	t.Helper()

	return service.Subscribe(1, url+"/without_self_link")
}

func thenSubscribeShouldBeSavedCorrectly(t *testing.T, feed *Feed, err error, url string) {
	t.Helper()
	a := assert.New(t)

	expectedFeed := getTestFeed()
	expectedFeed.FeedURL = url + "/without_self_link"

	a.NoError(err)
	a.Equal(expectedFeed, feed)
}

// runTestFeedServer run server that response with texture file data, returns test server URL
func runTestFeedServer(t *testing.T) string {
	t.Helper()

	urlPathToFixturePathMap := map[string]string{
		"/rss":               "testdata/rss.xml",
		"/atom":              "testdata/atom.xml",
		"/without_self_link": "testdata/rss_without_self_link.xml",
	}

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		urlPath := r.URL.Path
		if filePath, ok := urlPathToFixturePathMap[urlPath]; ok {
			file, err := ioutil.ReadFile(filePath)

			if err != nil {
				t.Fatal(err)
			}

			w.Header().Add("Content-Type", "application/xml")
			_, err = w.Write(file)

			if err != nil {
				t.Fatal(err)
			}
			return
		}

		t.Fatalf("unknown path %s\n", urlPath)
	}))

	t.Cleanup(func() {
		ts.Close()
	})

	return ts.URL
}

func TestService_SubscribeDuplicate(t *testing.T) {
	a := assert.New(t)
	srv := setupServiceForSubscribeTest()

	_, err := srv.Subscribe(1, "http://example.com/feed")

	a.NoError(err)

	_, err = srv.Subscribe(1, "http://example.com/feed")
	if a.Error(err) {
		a.Equal(ErrAlreadySubscribed, err)
	}
}

func setupServiceForSubscribeTest() *Service {
	store := &storeMock{}

	store.On("Insert", mock.Anything).Return(nil).Times(1)
	store.On("Insert", mock.Anything).Return(ErrAlreadySubscribed)

	store.On("FindAll", mock.Anything).Return([]*Subscription{}, nil).Times(1)
	store.On("FindAll", mock.Anything).Return([]*Subscription{{
		ChatID:               1,
		Name:                 "Feed Title",
		SubscriptionFeedURL:  "http://example.com/feed",
		LastUpdatesFetchedAt: time.Now(),
	}}, nil).Times(2)

	srv := New(store, func(url string) (feed *Feed, err error) {
		return getTestFeed(), nil
	}, FetcherFunc(func(url string) (string, error) { return "", nil }))

	return srv
}

func TestService_FetchSubscriptionUpdatesBeforeTime(t *testing.T) {
	srv := setupServiceForSubscriptionUpdatesTest()
	a := assert.New(t)

	channel, err := srv.FetchSubscriptionUpdatesBeforeTime(time13h)
	a.NoError(err)

	count := 0
	for upd := range channel {
		count++
		a.Equal(Update{
			Subscription: Subscription{
				ChatID:               1,
				Name:                 "Feed Title",
				SubscriptionFeedURL:  "http://example.com/feed",
				LastUpdatesFetchedAt: time12h,
			},
			Feed: Feed{
				Title:       "Feed Title",
				Description: "Description",
				URL:         "http://example.com",
				FeedURL:     "http://example.com/feed",
				Entries: []*Entry{
					{
						Title:       "Entry",
						Description: "Desc",
						PublishedAt: &time12h,
						URL:         "http://example.com/entry",
					},
					{
						Title:       "New Entry",
						Description: "Desc",
						PublishedAt: &time12h30m,
						URL:         "http://example.com/new-entry",
					},
				},
			},
			NewEntries: []*Entry{{
				Title:       "New Entry",
				Description: "Desc",
				PublishedAt: &time12h30m,
				URL:         "http://example.com/new-entry",
			}},
		}, upd)
	}

	a.Equal(1, count, "expected exactly one update")
}

func setupServiceForSubscriptionUpdatesTest() *Service {
	store := &storeMock{}

	store.
		On("FindAllFetchedBefore", mock.AnythingOfType("time.Time")).
		Return([]*Subscription{
			{
				ChatID:               1,
				Name:                 "Feed Title",
				SubscriptionFeedURL:  "http://example.com/feed",
				LastUpdatesFetchedAt: time12h,
			},
		}, nil)

	store.
		On("Update", mock.AnythingOfType("*feed.Subscription")).
		Return(nil)

	srv := New(store,
		func(content string) (feed *Feed, err error) {
			return &Feed{
				Title:       "Feed Title",
				Description: "Description",
				URL:         "http://example.com",
				FeedURL:     content,
				Entries: []*Entry{
					{
						Title:       "Entry",
						Description: "Desc",
						PublishedAt: &time12h,
						URL:         "http://example.com/entry",
					},
					{
						Title:       "New Entry",
						Description: "Desc",
						PublishedAt: &time12h30m,
						URL:         "http://example.com/new-entry",
					},
				},
			}, nil
		},
		FetcherFunc(func(url string) (string, error) {
			return "", nil
		}),
	)

	return srv
}

func TestService_FetchSubscriptionUpdatesBeforeTime_UpdateStore(t *testing.T) {
	store := &storeMock{}

	store.
		On("FindAllFetchedBefore", mock.AnythingOfType("time.Time")).
		Return([]*Subscription{
			{
				ChatID:               1,
				Name:                 "Feed Title",
				SubscriptionFeedURL:  "http://example.com/feed",
				LastUpdatesFetchedAt: time12h,
			},
		}, nil)

	store.
		On("Update", mock.AnythingOfType("*feed.Subscription")).
		Return(nil)

	srv := New(store,
		func(content string) (feed *Feed, err error) {
			return &Feed{
				Title:       "Feed Title",
				Description: "Description",
				URL:         "http://example.com",
				FeedURL:     content,
				Entries: []*Entry{
					{
						Title:       "Entry",
						Description: "Desc",
						PublishedAt: &time12h,
						URL:         "http://example.com/entry",
					},
					{
						Title:       "New Entry",
						Description: "Desc",
						PublishedAt: &time12h30m,
						URL:         "http://example.com/new-entry",
					},
				},
			}, nil
		},
		FetcherFunc(func(url string) (string, error) {
			return "", nil
		}),
	)
	a := assert.New(t)

	channel, err := srv.FetchSubscriptionUpdatesBeforeTime(time13h)
	a.NoError(err)

	for range channel {
	}

	store.AssertCalled(t, "Update", mock.AnythingOfType("*feed.Subscription"))
}

func TestService_fetchSubscriptionUpdate_ParseError(t *testing.T) {
	subscription := givenSubscriptionWithIncorrectFeedFormat(t)
	update := whenFetchUpdatesStarted(t, subscription)
	thenUpdateContainsError(t, &update)
}

func givenSubscriptionWithIncorrectFeedFormat(t *testing.T) *Subscription {
	t.Helper()

	return &Subscription{
		ChatID:               1,
		Name:                 "Incorrect Feed Format",
		SubscriptionFeedURL:  "http://incorrect-feed.com/feed",
		LastUpdatesFetchedAt: time.Now(),
	}
}

func whenFetchUpdatesStarted(t *testing.T, subscription *Subscription) Update {
	t.Helper()

	srv := New(new(storeMock), func(content string) (feed *Feed, err error) {
		return nil, fmt.Errorf("Unknown feed format")
	}, FetcherFunc(func(_ string) (string, error) { return "content", nil }))

	return srv.fetchSubscriptionUpdate(subscription)
}

func thenUpdateContainsError(t *testing.T, update *Update) {
	t.Helper()
	a := assert.New(t)

	a.Error(update.Error)
}

func TestService_GetSubscriptions(t *testing.T) {
	store := givenStoreWithFewSubscriptions(t)
	subscriptions, err := whenGetSubscriptionsCalled(t, store)
	thenChatSubscriptionsFromStoreReturn(t, subscriptions, err)
}

func givenStoreWithFewSubscriptions(t *testing.T) Store {
	t.Helper()

	subscriptions := []*Subscription{
		{
			ChatID:               1,
			Name:                 "Feed Title 1",
			SubscriptionFeedURL:  "http://example.com/feed1",
			LastUpdatesFetchedAt: time12h,
		},
		{
			ChatID:               1,
			Name:                 "Feed Title 2",
			SubscriptionFeedURL:  "http://example.com/feed2",
			LastUpdatesFetchedAt: time12h,
		},
	}

	m := new(storeMock)
	m.On("FindAll", int64(1)).Return(subscriptions, nil)

	return m
}

func whenGetSubscriptionsCalled(t *testing.T, store Store) ([]*Subscription, error) {
	t.Helper()

	service := New(store, ParseRSS, NewDefaultFetcher())

	return service.GetSubscriptions(1)
}

func thenChatSubscriptionsFromStoreReturn(t *testing.T, subscriptions []*Subscription, err error) {
	t.Helper()
	a := assert.New(t)

	expectedSubscriptions := []*Subscription{
		{
			ChatID:               1,
			Name:                 "Feed Title 1",
			SubscriptionFeedURL:  "http://example.com/feed1",
			LastUpdatesFetchedAt: time12h,
		},
		{
			ChatID:               1,
			Name:                 "Feed Title 2",
			SubscriptionFeedURL:  "http://example.com/feed2",
			LastUpdatesFetchedAt: time12h,
		},
	}

	a.NoError(err)
	a.Equal(expectedSubscriptions, subscriptions)
}
