package feed

import (
	"database/sql"
	"gofeedbot/internal/app/log"

	migrate "github.com/rubenv/sql-migrate"
)

var migrations = migrate.MemoryMigrationSource{
	Migrations: []*migrate.Migration{
		{
			Id: "1_initialize",
			Up: []string{
				`
CREATE TABLE IF NOT EXISTS subscription (
chat_id INTEGER NOT NULL,
name TEXT,
url TEXT NOT NULL,
fetched_at TEXT,
PRIMARY KEY (chat_id, url)
);`,
			},
			Down: []string{"DROP TABLE subscription"},
		},
		{
			Id: "2_collate_nocase",
			Up: []string{
				`
CREATE TABLE subscription_tmp (
	chat_id INTEGER NOT NULL,
	name TEXT,
	url TEXT NOT NULL collate nocase,
	fetched_at TEXT,
	PRIMARY KEY (chat_id, url)
);

INSERT INTO subscription_tmp 
	SELECT * from subscription GROUP BY chat_id, LOWER(url);

DROP TABLE subscription;

ALTER TABLE subscription_tmp RENAME TO subscription;
`,
			},
			Down: []string{
				`
CREATE TABLE subscription_tmp (
	chat_id INTEGER NOT NULL,
	name TEXT,
	url TEXT NOT NULL,
	fetched_at TEXT,
	PRIMARY KEY (chat_id, url)
);

INSERT INTO subscription_tmp SELECT * from subscription;

DROP TABLE subscription;

ALTER TABLE subscription_tmp RENAME TO subscription;
`,
			},
		},
		{
			Id: "3-remove-corrupted-subscriptions",
			Up: []string{
				"DELETE FROM subscription WHERE url NOT LIKE 'http%'",
			},
		},
	},
}

func NewDB(dataSource SQLiteDataSource) (*sql.DB, error) {
	return sql.Open("sqlite3", string(dataSource))
}

func initDB(db *sql.DB, logger *log.Logger) error {
	n, err := migrate.Exec(db, "sqlite3", migrations, migrate.Up)
	logger.Debug(log.Fields{
		"method":                   "feed::initDB",
		"error":                    err,
		"applied_migrations_count": n,
	}, "migration ended")
	return err
}
