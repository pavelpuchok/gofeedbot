package feed

import (
	"errors"
	"fmt"
)

var ErrAlreadySubscribed = errors.New("already subscribed")

type ErrFetchURL struct {
	err error
	url string
}

func (e ErrFetchURL) Error() string {
	return fmt.Sprintf("fetch failed. url:%s error: %s", e.url, e.err)
}

func (e ErrFetchURL) Unwrap() error {
	return e.err
}

type ErrParseContent struct {
	err error
}

func (e ErrParseContent) Error() string {
	return fmt.Sprintf("content parse error: %s", e.err)
}

func (e ErrParseContent) Unwrap() error {
	return e.err
}

type ErrInternal struct {
	err error
}

func (e ErrInternal) Error() string {
	return fmt.Sprintf("internal error %s", e.err)
}
