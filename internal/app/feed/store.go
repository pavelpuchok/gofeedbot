package feed

import "time"

type Store interface {
	// Insert saves subscription in storage
	//
	// Returns ErrAlreadySubscribed if subscription
	// with same URL and ChatID already exists.
	//
	// ErrInternal returned if any other error occurred
	Insert(subscription *Subscription) error

	// Update subscription data (except for URL
	// and ChatID fields) in storage.
	//
	// Update will return ErrInternal on any error
	Update(subscription *Subscription) error

	// FindAll returns all subscriptions with specified ChatID
	FindAll(chatID int64) ([]*Subscription, error)

	// FindAllFetchedBefore returns all subscription where
	// last update time < specified time
	//
	// FindAllFetchedBefore returns ErrInternal on any error
	// occurred
	FindAllFetchedBefore(time time.Time) ([]*Subscription, error)
}
