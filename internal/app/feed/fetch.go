package feed

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

type Fetcher interface {
	Fetch(url string) (string, error)
}

type DefaultFetcher struct {
	Client *http.Client
}

func NewDefaultFetcher() *DefaultFetcher {
	return &DefaultFetcher{
		Client: &http.Client{},
	}
}

func (f *DefaultFetcher) Fetch(url string) (string, error) {
	response, err := f.Client.Get(url)
	if err != nil {
		return "", nil
	}

	if response.StatusCode != 200 {
		return "", fmt.Errorf("request failed. Status Code: %d", response.StatusCode)
	}

	content, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	return string(content), err
}

type FetcherFunc func(url string) (string, error)

func (f FetcherFunc) Fetch(url string) (string, error) {
	return f(url)
}
