package feed

import "time"

func getTestFeed() *Feed {
	t := time.Unix(1570048800, 0).UTC() // 2019-10-02T20:40:00+00:00
	return &Feed{
		Title:       "Feed Title",
		URL:         "http://example.com/",
		FeedURL:     "http://example.com/feed",
		Description: "Feed Description",
		Entries: []*Entry{
			{
				Title:       "Entry Title",
				Description: "Entry Description",
				PublishedAt: &t,
				URL:         "http://example.com/slug-1.html",
			},
		},
	}
}
