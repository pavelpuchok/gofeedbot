package feed

import "time"

type Entry struct {
	Title       string
	Description string
	PublishedAt *time.Time
	URL         string
}
