package feed

import (
	"sync"
	"time"
)

type Service struct {
	subscriptions Store
	parse         ParseFunc
	fetcher       Fetcher
}

func New(store Store, parseFunc ParseFunc, fetcher Fetcher) *Service {
	return &Service{
		subscriptions: store,
		parse:         parseFunc,
		fetcher:       fetcher,
	}
}

// Subscribe create feed subscription for specified chat
// Subscribe returns an ErrAlreadySubscribed when chat already subscribed to it
// If fetching or parsing fails, Subscribe returns an error
// wrapping ErrFetchURL or ErrParseContent respectively
func (s *Service) Subscribe(chatID int64, url string) (*Feed, error) {
	feed, err := s.fetchAndParse(url)
	if err != nil {
		return feed, err
	}

	sub := &Subscription{
		ChatID:               chatID,
		Name:                 feed.Title,
		SubscriptionFeedURL:  feed.FeedURL,
		LastUpdatesFetchedAt: time.Now(),
	}

	err = s.subscriptions.Insert(sub)
	return feed, err
}

func (s *Service) fetchAndParse(url string) (*Feed, error) {
	content, err := s.fetcher.Fetch(url)

	if err != nil {
		return nil, ErrFetchURL{err: err, url: url}
	}

	feed, err := s.parse(content)

	if err != nil {
		return nil, ErrParseContent{err: err}
	}

	if feed.FeedURL == "" {
		feed.FeedURL = url
	}

	return feed, nil
}

type Update struct {
	Subscription Subscription
	Feed         Feed
	NewEntries   []*Entry
	Error        error
}

// FetchSubscriptionUpdatesBeforeTime starts fetching updates for subscriptions
// that was last updated before specified time. Returns a buffered channel through which
// updates will be sent.
func (s *Service) FetchSubscriptionUpdatesBeforeTime(time time.Time) (<-chan Update, error) {
	subs, err := s.subscriptions.FindAllFetchedBefore(time)

	if err != nil {
		return nil, err
	}

	return s.fetchSubscriptionUpdates(subs), nil
}

func (s *Service) fetchSubscriptionUpdates(subscriptions []*Subscription) <-chan Update {
	updatesChannel := make(chan Update)
	wg := sync.WaitGroup{}
	wg.Add(len(subscriptions))

	for _, sub := range subscriptions {
		go func(sub *Subscription) {
			update := s.fetchSubscriptionUpdate(sub)

			updatesChannel <- update
			wg.Done()
		}(sub)
	}

	go func() {
		wg.Wait()
		close(updatesChannel)
	}()

	return updatesChannel
}

func (s *Service) fetchSubscriptionUpdate(subscription *Subscription) Update {
	feed, err := s.fetchAndParse(subscription.SubscriptionFeedURL)

	if err != nil {
		return Update{
			Subscription: *subscription,
			Feed:         Feed{},
			NewEntries:   nil,
			Error:        err,
		}
	}

	updatedSub := *subscription
	updatedSub.LastUpdatesFetchedAt = time.Now()

	if err := s.subscriptions.Update(&updatedSub); err != nil {
		return Update{
			Subscription: *subscription,
			Feed:         *feed,
			NewEntries:   nil,
			Error:        err,
		}
	}

	newEntries := s.filterNewEntries(feed.Entries, subscription.LastUpdatesFetchedAt)

	return Update{
		Subscription: *subscription,
		Feed:         *feed,
		NewEntries:   newEntries,
		Error:        nil,
	}
}

func (s *Service) filterNewEntries(entries []*Entry, lastUpdatesFetchTime time.Time) []*Entry {
	newEntries := make([]*Entry, 0)

	for _, entry := range entries {
		if entry.PublishedAt.After(lastUpdatesFetchTime) {
			newEntries = append(newEntries, entry)
		}
	}

	return newEntries
}

func (s *Service) GetSubscriptions(chatID int64) ([]*Subscription, error) {
	return s.subscriptions.FindAll(chatID)
}
