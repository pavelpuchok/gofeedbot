package feed

import (
	"time"

	"github.com/stretchr/testify/mock"
)

type storeMock struct {
	mock.Mock
}

func (s *storeMock) Insert(subscription *Subscription) error {
	args := s.Called(subscription)
	return args.Error(0)
}

func (s *storeMock) Update(subscription *Subscription) error {
	args := s.Called(subscription)
	return args.Error(0)
}

func (s *storeMock) FindAll(chatID int64) ([]*Subscription, error) {
	args := s.Called(chatID)
	return args.Get(0).([]*Subscription), args.Error(1)
}

func (s *storeMock) FindAllFetchedBefore(time time.Time) ([]*Subscription, error) {
	args := s.Called(time)
	return args.Get(0).([]*Subscription), args.Error(1)
}
