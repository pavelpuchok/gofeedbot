package feed

type Feed struct {
	Title       string
	Description string
	URL         string
	FeedURL     string
	Entries     []*Entry
}
