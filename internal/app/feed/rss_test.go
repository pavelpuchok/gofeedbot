package feed

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_parseRSS_atom(t *testing.T) {
	a := assert.New(t)

	feed, err := ParseRSS(readFixture(t, "testdata/atom.xml"))

	a.NoError(err)
	a.Equal(getTestFeed(), feed)
}

func Test_parseRSS_rss(t *testing.T) {
	a := assert.New(t)

	feed, err := ParseRSS(readFixture(t, "testdata/rss.xml"))

	a.NoError(err)
	a.Equal(getTestFeed(), feed)
}

func readFixture(t *testing.T, filename string) string {
	bytes, err := ioutil.ReadFile(filename)

	if err != nil {
		t.Fatal(err)
	}

	return string(bytes)
}
