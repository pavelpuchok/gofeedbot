package feed

import "time"

type Subscription struct {
	ChatID               int64
	Name                 string
	SubscriptionFeedURL  string
	LastUpdatesFetchedAt time.Time
}
