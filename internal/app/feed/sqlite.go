package feed

import (
	"database/sql"
	"gofeedbot/internal/app/log"
	"time"

	"github.com/mattn/go-sqlite3"

	_ "github.com/mattn/go-sqlite3" // for use with database/sql package
)

type SQLiteStore struct {
	db *sql.DB
}

type SQLiteDataSource string

func NewSQLiteStore(db *sql.DB, logger *log.Logger) (*SQLiteStore, error) {
	err := initDB(db, logger)
	return &SQLiteStore{db: db}, err
}

func (s *SQLiteStore) Insert(subscription *Subscription) error {
	dt := s.dateTimeToDBFormat(subscription.LastUpdatesFetchedAt)

	_, err := s.db.Exec(`INSERT INTO subscription (chat_id, name, url, fetched_at) VALUES (?,?,?,?)`,
		subscription.ChatID, subscription.Name, subscription.SubscriptionFeedURL, dt)

	if err != nil {
		if e, ok := err.(sqlite3.Error); ok && e.Code == sqlite3.ErrConstraint {
			return ErrAlreadySubscribed

		}

		return ErrInternal{err}
	}

	return nil
}

func (s *SQLiteStore) Update(subscription *Subscription) error {
	dt := s.dateTimeToDBFormat(subscription.LastUpdatesFetchedAt)

	_, err := s.db.Exec(`UPDATE subscription SET name=?, fetched_at=? WHERE chat_id=? AND url=?`,
		subscription.Name, dt, subscription.ChatID, subscription.SubscriptionFeedURL)

	if err != nil {
		return ErrInternal{err}
	}

	return nil
}

func (s *SQLiteStore) FindAll(chatID int64) ([]*Subscription, error) {
	rows, err := s.db.Query(`SELECT chat_id, name, url, fetched_at FROM subscription WHERE chat_id=?`, chatID)
	if err != nil {
		return nil, ErrInternal{err}
	}
	defer rows.Close()

	subscriptions, err := s.scanSubscriptions(rows)

	if err != nil {
		return nil, ErrInternal{err}
	}

	return subscriptions, nil
}

func (s *SQLiteStore) FindAllFetchedBefore(datetime time.Time) ([]*Subscription, error) {
	rows, err := s.db.Query(`SELECT chat_id, name, url, fetched_at FROM subscription WHERE DATETIME(fetched_at) < DATETIME(?)`, s.dateTimeToDBFormat(datetime))
	if err != nil {
		return nil, ErrInternal{err}
	}
	defer rows.Close()

	subscriptions, err := s.scanSubscriptions(rows)

	if err != nil {
		return nil, ErrInternal{err}
	}

	return subscriptions, err
}

func (s *SQLiteStore) scanSubscriptions(rows *sql.Rows) ([]*Subscription, error) {
	subscriptions := []*Subscription{}

	for rows.Next() {
		sub := &Subscription{}
		dt := ""
		if err := rows.Scan(&sub.ChatID, &sub.Name, &sub.SubscriptionFeedURL, &dt); err != nil {
			return subscriptions, err
		}

		t, err := s.dateTimeFromDBFormat(dt)
		if err != nil {
			return subscriptions, err
		}

		sub.LastUpdatesFetchedAt = t

		subscriptions = append(subscriptions, sub)
	}

	return subscriptions, nil
}

func (s *SQLiteStore) dateTimeToDBFormat(dt time.Time) string {
	return dt.UTC().Format(time.RFC3339)
}

func (s *SQLiteStore) dateTimeFromDBFormat(dt string) (time.Time, error) {
	t, err := time.Parse(time.RFC3339, dt)
	if err != nil {
		return t, err
	}

	return t.Local(), nil
}
