package feed

import (
	"github.com/mmcdole/gofeed"
)

type ParseFunc func(content string) (*Feed, error)

func ParseRSS(content string) (*Feed, error) {
	parser := gofeed.NewParser()
	rssFeed, err := parser.ParseString(content)

	if err != nil {
		return nil, err
	}

	feed := &Feed{
		Title:       rssFeed.Title,
		Description: rssFeed.Description,
		URL:         rssFeed.Link,
		FeedURL:     rssFeed.FeedLink,
	}

	for _, item := range rssFeed.Items {
		entry := &Entry{
			Title:       item.Title,
			Description: item.Description,
			URL:         item.Link,
		}

		if item.PublishedParsed != nil {
			entry.PublishedAt = item.PublishedParsed
		} else if item.UpdatedParsed != nil {
			entry.PublishedAt = item.UpdatedParsed
		}

		feed.Entries = append(feed.Entries, entry)
	}

	return feed, nil
}
