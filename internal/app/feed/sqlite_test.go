package feed

import (
	"database/sql"
	"gofeedbot/internal/app/log"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewSQLiteStore(t *testing.T) {
	db := prepareTestDB(t)
	a := assert.New(t)

	store, err := NewSQLiteStore(db, log.New(log.FatalLevel))

	a.NoError(err)
	a.Implements((*Store)(nil), store)
}

func TestSQLiteStore_Insert(t *testing.T) {
	store := setupStoreForTest(t)
	a := assert.New(t)

	subscription := getSubscriptionForTest()

	// Save
	err := store.Insert(subscription)
	a.NoError(err)

	// Validate save
	result := readSubscriptionFromDb(store.db)

	a.Equal(subscription, result)
}

func TestSQLiteStore_Update(t *testing.T) {
	store := setupStoreForUpdateTest(t)
	a := assert.New(t)

	subscription := &Subscription{
		ChatID:               13212,
		Name:                 "First sub",
		SubscriptionFeedURL:  "http://example.org/feed",
		LastUpdatesFetchedAt: time.Now().Truncate(time.Second),
	}

	err := store.Update(subscription)
	a.NoError(err)

	// Validate save
	result := readSubscriptionFromDb(store.db)

	a.Equal(subscription, result)
}

func setupStoreForUpdateTest(t *testing.T) *SQLiteStore {
	store := setupStoreForTest(t)

	existingSubs := []*Subscription{
		{
			ChatID:               13212,
			Name:                 "First sub",
			SubscriptionFeedURL:  "http://example.org/feed",
			LastUpdatesFetchedAt: time.Unix(1577836800, 0).Local(), // 2020-01-01T00:00:00
		},
		{
			ChatID:               13212,
			Name:                 "Second Sub",
			SubscriptionFeedURL:  "http://second-sub.org/feed",
			LastUpdatesFetchedAt: time.Unix(1577836800, 0).Local(), // 2020-01-01T00:00:00
		},
	}

	for _, sub := range existingSubs {
		if err := store.Insert(sub); err != nil {
			t.Fatal(err)
		}
	}

	return store
}

func readSubscriptionFromDb(db *sql.DB) *Subscription {
	dt := ""
	result := &Subscription{}
	_ = db.
		QueryRow(`SELECT chat_id, name, url, fetched_at FROM subscription WHERE chat_id=? AND url=?`,
			13212,                     // ID from test subscription
			"http://example.org/feed", // URL from test subscription
		).
		Scan(&result.ChatID, &result.Name, &result.SubscriptionFeedURL, &dt)

	fetchTime, _ := time.Parse(time.RFC3339, dt)
	result.LastUpdatesFetchedAt = fetchTime.Local()

	return result
}

func TestSQLiteStore_InsertDuplicate(t *testing.T) {
	store := setupStoreForTest(t)
	a := assert.New(t)

	subscription := getSubscriptionForTest()

	// Save
	err := store.Insert(subscription)
	a.NoError(err)
	err = store.Insert(subscription)

	if a.Error(err) {
		a.Equal(ErrAlreadySubscribed, err)
	}

	// should ignore case
	subscription.SubscriptionFeedURL = strings.ToUpper(subscription.SubscriptionFeedURL)
	err = store.Insert(subscription)

	if a.Error(err) {
		a.Equal(ErrAlreadySubscribed, err)
	}
}

func TestSQLiteStore_FindSubscriptions(t *testing.T) {
	store := setupStoreForTest(t)
	a := assert.New(t)

	expected := getSubscriptionForTest()

	// Empty Result
	result, err := store.FindAll(expected.ChatID)
	a.NoError(err)
	a.Len(result, 0)

	// Add subscription
	a.NoError(store.Insert(expected))

	result, err = store.FindAll(expected.ChatID)
	a.NoError(err)
	a.Len(result, 1)
	a.Equal(expected, result[0])
}

func getSubscriptionForTest() *Subscription {
	return &Subscription{
		ChatID:               13212,
		Name:                 "ExampleFeed",
		SubscriptionFeedURL:  "http://example.org/feed",
		LastUpdatesFetchedAt: time.Now().Truncate(time.Second),
	}
}

func TestSQLiteStore_FindAllFetchedBefore(t *testing.T) {
	firstTime := time.Unix(1577880000, 0)  // 2020-01-01T12:00:00+00:00
	secondTime := time.Unix(1577883600, 0) // 2020-01-01T13:00:00+00:00

	tests := []struct {
		name     string
		expected []*Subscription
		datetime time.Time
	}{
		{
			name:     "empty result",
			expected: []*Subscription{},
			datetime: firstTime,
		},
		{
			name: "single subscription entry",
			expected: []*Subscription{
				{
					ChatID:               1,
					Name:                 "ExampleFeed",
					SubscriptionFeedURL:  "http://example.org/feed",
					LastUpdatesFetchedAt: firstTime,
				},
			},
			datetime: firstTime.Add(1 * time.Minute),
		},
		{
			name: "multiple subscriptions",
			expected: []*Subscription{
				{
					ChatID:               1,
					Name:                 "ExampleFeed",
					SubscriptionFeedURL:  "http://example.org/feed",
					LastUpdatesFetchedAt: firstTime,
				},
				{
					ChatID:               2,
					Name:                 "ExampleFeed2",
					SubscriptionFeedURL:  "http://example.org/feed2",
					LastUpdatesFetchedAt: secondTime,
				},
			},
			datetime: secondTime.Add(1 * time.Minute),
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			store := setupStoreForFindAllFetchedBeforeTest(t)
			subscriptions, err := store.FindAllFetchedBefore(tt.datetime)

			a := assert.New(t)
			a.NoError(err)
			a.Equal(tt.expected, subscriptions)
		})
	}
}

func setupStoreForFindAllFetchedBeforeTest(t *testing.T) *SQLiteStore {
	store := setupStoreForTest(t)

	firstTime := time.Unix(1577880000, 0)  // 2020-01-01T12:00:00+00:00
	secondTime := time.Unix(1577883600, 0) // 2020-01-01T13:00:00+00:00

	defaultSubscriptions := []*Subscription{
		{
			ChatID:               1,
			Name:                 "ExampleFeed",
			SubscriptionFeedURL:  "http://example.org/feed",
			LastUpdatesFetchedAt: firstTime,
		},
		{
			ChatID:               2,
			Name:                 "ExampleFeed2",
			SubscriptionFeedURL:  "http://example.org/feed2",
			LastUpdatesFetchedAt: secondTime,
		},
	}

	for _, sub := range defaultSubscriptions {
		if err := store.Insert(sub); err != nil {
			t.Fatal(err)
		}
	}

	return store
}

func setupStoreForTest(t *testing.T) *SQLiteStore {
	db := prepareTestDB(t)
	store, err := NewSQLiteStore(db, log.New(log.FatalLevel))
	if err != nil {
		t.Fatal(err)
	}
	return store
}

func prepareTestDB(t *testing.T) *sql.DB {
	t.Helper()

	db, err := sql.Open("sqlite3", ":memory:")

	if err != nil {
		t.Fatal(err)
	}

	t.Cleanup(func() {
		db.Close()
	})

	return db
}
