package feed

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewDefaultFetcher(t *testing.T) {
	a := assert.New(t)

	a.Implements((*Fetcher)(nil), NewDefaultFetcher())
}

func TestFetcherFunc(t *testing.T) {
	a := assert.New(t)

	ff := FetcherFunc(
		func(u string) (string, error) {
			return "", nil
		},
	)

	a.Implements((*Fetcher)(nil), ff)
}
