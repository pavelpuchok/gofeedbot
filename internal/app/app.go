package app

import (
	"gofeedbot/internal/app/bot"
	"gofeedbot/internal/app/feed"
	"gofeedbot/internal/app/format"
	"gofeedbot/internal/app/log"
	"gofeedbot/internal/app/schedule"
	"time"
)

const (
	errMessageInvalidLink       = "Is not a link to valid RSS/Atom feed"
	errMessageAlreadySubscribed = "You are already subscribed to this feed"
	errInternalError            = "Internal error. Please, try again later"
	errUnknownCommand           = "Unknown command"
)

type App struct {
	bot                bot.Bot
	feed               *feed.Service
	scheduler          schedule.Scheduler
	feedUpdateDuration time.Duration
	logger             *log.Logger
}

func New(bot bot.Bot, service *feed.Service, updateFrequency time.Duration, logger *log.Logger) (*App, error) {
	return &App{
		bot:                bot,
		feed:               service,
		feedUpdateDuration: updateFrequency,
		logger:             logger,
	}, nil
}

func (a *App) Run() {
	a.logger.Info(log.Fields{
		"method":         "App.Run",
		"updateDuration": a.feedUpdateDuration,
	}, "app start")
	a.scheduler = schedule.NewTickScheduler(a.feedUpdateDuration, a.updateFeed)
	a.scheduler.Start()
	a.listenBotUpdates()
}

func (a *App) updateFeed() {
	lastUpdateTime := time.Now().Add(-1 * a.feedUpdateDuration)
	channel, err := a.feed.FetchSubscriptionUpdatesBeforeTime(lastUpdateTime)

	if err != nil {
		a.logger.Error(log.Fields{
			"method": "App.updateFeed",
			"error":  err,
		}, "fetch subscription updates failed")
		return
	}

	for update := range channel {
		if update.Error != nil {
			a.logger.Error(log.Fields{
				"method": "App.updateFeed",
				"error":  update.Error,
				"url":    update.Subscription.SubscriptionFeedURL,
			}, "subscription update error")
			continue
		}

		a.sendFeedEntries(update.Subscription.ChatID, update.NewEntries)
	}
}

func (a *App) sendFeedEntries(chatID int64, entries []*feed.Entry) {
	for _, entry := range entries {
		err := a.bot.Send(chatID, format.FeedEntry(entry))
		if err != nil {
			a.logger.Error(log.Fields{
				"method": "App.sendFeedEntries",
				"chatID": chatID,
				"error":  err,
			}, "sending feed entry failed")
		}
	}
}

func (a *App) listenBotUpdates() {
	for upd := range a.bot.ListenMessages() {
		a.logger.Debug(log.Fields{
			"method":    "App.listenBotUpdates",
			"chatID":    upd.ChatID,
			"text":      upd.Text,
			"messageID": upd.MessageID,
		}, "update received")
		a.processUpdate(upd)
	}
}

func (a *App) processUpdate(message *bot.MessageUpdate) {
	if message.IsCommand {
		a.processCommandMessage(message)
		return
	}

	a.processSubscribeMessage(message)
}

func (a *App) processCommandMessage(message *bot.MessageUpdate) {
	if message.Command == "list" {
		a.processListCommand(message)
		return
	}

	a.logger.Error(log.Fields{
		"method":  "App.processCommandMessage",
		"chatID":  message.ChatID,
		"command": message.Command,
	}, "unknown command received")
	a.sendReplyWithLog(message.ChatID, message.MessageID, errUnknownCommand)
}

func (a *App) processListCommand(message *bot.MessageUpdate) {
	subs, err := a.feed.GetSubscriptions(message.ChatID)

	if err != nil {
		a.logger.Error(log.Fields{
			"method": "App.processListCommand",
			"chatID": message.ChatID,
			"error":  err,
		}, "getting subscriptions failed")
		a.sendReplyWithLog(message.ChatID, message.MessageID, errInternalError)
	}

	a.sendReplyWithLog(message.ChatID, message.MessageID, format.SubscriptionsList(subs))
}

func (a *App) processSubscribeMessage(message *bot.MessageUpdate) {
	if len(message.URLEntities) == 0 {
		a.sendReplyWithLog(message.ChatID, message.MessageID, errMessageInvalidLink)
		return
	}

	feedURL := message.URLEntities[0]
	f, err := a.feed.Subscribe(message.ChatID, feedURL)
	if err != nil {
		if err == feed.ErrAlreadySubscribed {
			a.sendReplyWithLog(message.ChatID, message.MessageID, errMessageAlreadySubscribed)
			return
		}

		a.logger.Error(log.Fields{
			"method": "App.processSubscribeMessage",
			"chatID": message.ChatID,
			"url":    feedURL,
			"error":  err,
		}, "subscribe failed")
		a.sendReplyWithLog(message.ChatID, message.MessageID, errMessageInvalidLink)
		return
	}

	a.sendReplyWithLog(message.ChatID, message.MessageID, format.Feed(f))
}

func (a *App) sendReplyWithLog(chatID int64, messageID int, text string) {
	err := a.bot.SendReply(chatID, messageID, text)
	if err != nil {
		a.logger.Error(log.Fields{
			"method":         "App.sendReplyWithLog",
			"error":          err,
			"chatID":         chatID,
			"replyMessageID": messageID,
			"messageText":    text,
		}, "send reply failed")
	}
}
