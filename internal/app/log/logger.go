package log

import (
	"github.com/sirupsen/logrus"
)

type Level uint8

const (
	FatalLevel Level = iota
	ErrorLevel
	InfoLevel
	DebugLevel
)

type Fields map[string]interface{}

type Logger struct {
	log *logrus.Logger
}

// Debug writes log entry with debug level
// Debug should be used only for low-level
// data that can help on debugging problems
func (l Logger) Debug(fields Fields, message string) {
	l.log.WithFields(logrus.Fields(fields)).Debug(message)
}

// Info prints log entry with info level
// Info level used for business log entries
func (l Logger) Info(fields Fields, message string) {
	l.log.WithFields(logrus.Fields(fields)).Info(message)
}

// Error writes log entry with error level
// Error should be used only for unexpected errors,
// that require attention asap
func (l Logger) Error(fields Fields, message string) {
	l.log.WithFields(logrus.Fields(fields)).Error(message)
}

// Fatal writes log entry with fatal level
// and exit
func (l Logger) Fatal(fields Fields, message string) {
	l.log.WithFields(logrus.Fields(fields)).Fatal(message)
}

func New(level Level) *Logger {
	log := logrus.New()
	log.Level = innerLevelToLogrusLevel(level)

	return &Logger{
		log: log,
	}
}

func innerLevelToLogrusLevel(level Level) logrus.Level {
	switch level {
	case FatalLevel:
		return logrus.PanicLevel
	case ErrorLevel:
		return logrus.ErrorLevel
	case InfoLevel:
		return logrus.InfoLevel
	default:
		return logrus.DebugLevel
	}
}
