//+build wireinject

package app

import (
	"fmt"
	"gofeedbot/internal/app/bot"
	"gofeedbot/internal/app/feed"
	"gofeedbot/internal/app/log"
	"net/http"
	"os"
	"time"

	"github.com/google/wire"
)

func NewFromEnvVars(logger *log.Logger) (*App, error) {
	wire.Build(
		getConfigurationFromEnvironmentVariables,
		wire.FieldsOf(new(*appConfiguration), "token", "dbDataSource", "updateFrequencyTime"),

		bot.NewTgBot,
		feed.NewDB,
		feed.NewSQLiteStore,
		feed.New,
		feed.NewDefaultFetcher,
		New,

		wire.Bind(new(bot.Bot), new(*bot.TgBot)),
		wire.Value(&http.Client{}),

		wire.Bind(new(feed.Store), new(*feed.SQLiteStore)),
		wire.Bind(new(feed.Fetcher), new(*feed.DefaultFetcher)),
		wire.Value(feed.ParseFunc(feed.ParseRSS)),
	)
	return &App{}, nil
}

type appConfiguration struct {
	token               bot.Token
	dbDataSource        feed.SQLiteDataSource
	updateFrequencyTime time.Duration
}

func getConfigurationFromEnvironmentVariables() (*appConfiguration, error) {
	token, err := getEnvVar("FEED_BOT_TOKEN")
	if err != nil {
		return nil, err
	}

	dataSource, err := getEnvVar("FEED_BOT_DB_DATA_SOURCE")
	if err != nil {
		return nil, err
	}

	updateFrequency, err := getEnvVar("FEED_BOT_UPDATE_FREQUENCY")
	if err != nil {
		return nil, err
	}

	updateFrequencyTime, err := time.ParseDuration(updateFrequency)
	if err != nil {
		return nil, err
	}

	return &appConfiguration{
		token:               bot.Token(token),
		dbDataSource:        feed.SQLiteDataSource(dataSource),
		updateFrequencyTime: updateFrequencyTime,
	}, nil
}

func getEnvVar(name string) (string, error) {
	value := os.Getenv(name)
	if value == "" {
		return "", fmt.Errorf("Missing %s environment variable", name)
	}
	return value, nil
}
