package format

import (
	"gofeedbot/internal/app/feed"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_formatFeed(t *testing.T) {
	tests := []struct {
		name string
		feed *feed.Feed
		want string
	}{
		{
			name: "Missing updates",
			feed: &feed.Feed{
				Title:       "Title",
				Description: "Description",
				URL:         "http://example.org",
				FeedURL:     "http://example.org/feed",
				Entries:     nil,
			},
			want: "[Title](http://example.org)\nDescription\n\n----------\n\nNo posts available",
		},
		{
			name: "Missing updates",
			feed: &feed.Feed{
				Title:       "Feed Title",
				Description: "Feed Description",
				URL:         "http://example.org",
				FeedURL:     "http://example.org/feed",
				Entries: []*feed.Entry{
					{
						Title:       "Entry Title2",
						Description: "Entry Description2",
						URL:         "http://example.org",
					},
					{
						Title:       "Entry Title1",
						Description: "Entry Description1",
						URL:         "http://example.org",
					},
				},
			},
			want: "[Feed Title](http://example.org)\nFeed Description\n\n----------\n\n[Entry Title2](http://example.org)\nEntry Description2",
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			a := assert.New(t)

			a.Equal(tt.want, Feed(tt.feed))
		})
	}
}
