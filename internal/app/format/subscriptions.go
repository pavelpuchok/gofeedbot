package format

import (
	"fmt"
	"gofeedbot/internal/app/feed"
	"strings"
)

func SubscriptionsList(subscriptions []*feed.Subscription) string {
	subscriptionsLength := len(subscriptions)
	if subscriptionsLength == 0 {
		return msgNoSubscriptions
	}

	result := fmt.Sprintf(msgSubscriptionListTitle, subscriptionsLength)

	items := make([]string, subscriptionsLength)
	for i, s := range subscriptions {
		listItemIndex := i + 1
		items[i] = fmt.Sprintf(msgSubscriptionListItem, listItemIndex, s.Name, s.SubscriptionFeedURL)
	}
	itemsList := strings.Join(items, "\n")

	return result + itemsList
}
