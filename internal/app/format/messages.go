package format

const (
	msgNoPostsAvailable = "No posts available"
	msgNoSubscriptions  = "You do not have any subscriptions. Send me a link to RSS/Atom feed to subscribe"
	// Title for list of subscriptions. Placeholders: number of subscriptions
	msgSubscriptionListTitle = "Number of subscriptions: %d\n\n"
	// Item for list of subscriptions. Placeholders: item index, item name, item link
	msgSubscriptionListItem = "%d. [%s](%s)"
)
