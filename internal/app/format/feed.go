package format

import (
	"fmt"
	"gofeedbot/internal/app/feed"
	"gofeedbot/internal/app/format/markdown"
)

func Feed(feed *feed.Feed) string {
	desc, err := markdown.FromHTML(feed.Description, feed.URL)

	if err != nil {
		desc = feed.Description
	}

	lastEntryText := msgNoPostsAvailable
	if len(feed.Entries) > 0 {
		lastEntryText = FeedEntry(feed.Entries[0])
	}

	return fmt.Sprintf("[%s](%s)\n%s\n\n----------\n\n%s", feed.Title, feed.URL, desc, lastEntryText)
}
