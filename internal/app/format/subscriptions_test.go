package format

import (
	"gofeedbot/internal/app/feed"
	"testing"
	"time"
)

func TestSubscriptionsList(t *testing.T) {
	tests := []struct {
		name          string
		subscriptions []*feed.Subscription
		want          string
	}{
		{
			name:          "empty list",
			subscriptions: make([]*feed.Subscription, 0),
			want:          msgNoSubscriptions,
		},
		{
			name: "few subscriptions",
			subscriptions: []*feed.Subscription{{
				ChatID:               1,
				Name:                 "Sub1",
				SubscriptionFeedURL:  "http://sub1.com/feed",
				LastUpdatesFetchedAt: time.Now(),
			}, {
				ChatID:               1,
				Name:                 "Sub2",
				SubscriptionFeedURL:  "http://sub2.com/feed",
				LastUpdatesFetchedAt: time.Now(),
			}},
			want: "Number of subscriptions: 2\n\n1. [Sub1](http://sub1.com/feed)\n2. [Sub2](http://sub2.com/feed)",
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			if got := SubscriptionsList(tt.subscriptions); got != tt.want {
				t.Errorf("SubscriptionsList() = %v, want %v", got, tt.want)
			}
		})
	}
}
