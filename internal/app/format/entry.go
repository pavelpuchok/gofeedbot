package format

import (
	"fmt"
	"gofeedbot/internal/app/feed"
	"gofeedbot/internal/app/format/markdown"
)

func FeedEntry(update *feed.Entry) string {
	desc, err := markdown.FromHTML(update.Description, update.URL)

	if err != nil {
		desc = update.Description
	}

	return fmt.Sprintf("[%s](%s)\n%s", update.Title, update.URL, desc)
}
