package markdown

import (
	md "github.com/Skarlso/html-to-markdown"
)

func FromHTML(html string, baseURL string) (string, error) {
	converter := md.NewConverter(baseURL, true, nil)
	converter.AddRules(getRules(baseURL)...)

	markdown, err := converter.ConvertString(html)

	if err != nil {
		return "", err
	}

	return markdown, nil
}
