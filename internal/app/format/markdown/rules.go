package markdown

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	md "github.com/Skarlso/html-to-markdown"
	"github.com/Skarlso/html-to-markdown/escape"
)

func getRules(baseURL string) []md.Rule {
	r := make([]md.Rule, len(builders))

	for i, buildRule := range builders {
		r[i] = buildRule(baseURL)
	}

	return r
}

var builders = []ruleBuilder{buildBoldReplaceRule, buildTitleReplaceRule, buildLinkReplaceRule, buildExtraSpaceReplaceRule, buileMonospaceReplaceRule}

type ruleBuilder func(baseUrl string) md.Rule

func buildBoldReplaceRule(_ string) md.Rule {
	return md.Rule{
		Filter: []string{"strong", "b"},
		Replacement: func(content string, selec *goquery.Selection, opt *md.Options) *string {
			trimmed := strings.TrimSpace(content)
			if trimmed == "" {
				return nil
			}

			return md.String("*" + content + "*")
		},
	}
}

func buildTitleReplaceRule(_ string) md.Rule {
	return md.Rule{
		Filter: []string{"h1", "h2", "h3", "h4", "h5", "h6"},
		Replacement: func(content string, sel *goquery.Selection, opt *md.Options) *string {
			return md.String(fmt.Sprintf("*%s*\n\n", content))
		},
	}
}

func buildLinkReplaceRule(baseURL string) md.Rule {
	return md.Rule{
		Filter: []string{"a"},
		AdvancedReplacement: func(content string, selec *goquery.Selection, opt *md.Options) (md.AdvancedResult, bool) {
			href, ok := selec.Attr("href")
			if !ok {
				return md.AdvancedResult{}, true
			}

			if strings.HasPrefix(href, "#") {
				href = baseURL + href
			}

			var title string
			if t, ok := selec.Attr("title"); ok {
				title = fmt.Sprintf(` "%s"`, t)
			}

			return md.AdvancedResult{
				Markdown: fmt.Sprintf("[%s](%s%s)", content, href, title),
			}, false
		},
	}
}

var tabR = regexp.MustCompile(`\t+`)

var multipleSpacesR = regexp.MustCompile(`  +`)

func buildExtraSpaceReplaceRule(_ string) md.Rule {
	return md.Rule{
		Filter: []string{"#text"},
		Replacement: func(content string, selec *goquery.Selection, opt *md.Options) *string {
			text := selec.Text()
			text = tabR.ReplaceAllString(text, " ")

			// replace multiple spaces by one space: dont accidentally make
			// normal text be indented and thus be a code block.
			text = multipleSpacesR.ReplaceAllString(text, " ")

			text = escape.Markdown(text)
			return &text
		},
	}
}

func buileMonospaceReplaceRule(_ string) md.Rule {
	return md.Rule{
		Filter: []string{"tt"},
		Replacement: func(content string, sel *goquery.Selection, opt *md.Options) *string {
			return md.String("`" + sel.Text() + "`")
		},
	}
}
