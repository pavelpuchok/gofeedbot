package markdown

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_FromHTML(t *testing.T) {
	tests := []struct {
		name    string
		in      string
		baseURL string
		out     string
	}{
		{
			name: "simple text",
			in:   "Just a <span>simple</span> and <b>bolded</b> text",
			out:  "Just a simple and *bolded* text",
		},
		{
			name: "header tags",
			in:   "<h1>First Header</h1><h2>Second Header</h2><h3>Third Header</h3><h4>Fourth Header</h4><h5>Fifth Header</h5><h6>Last Header</h6>Text",
			out:  "*First Header*\n\n*Second Header*\n\n*Third Header*\n\n*Fourth Header*\n\n*Fifth Header*\n\n*Last Header*\n\nText",
		},
		{
			name: "paragraph with a link",
			in:   "<p>paragraph with a <a href=\"example.org\"/>link</a></p>",
			out:  "paragraph with a [link](example.org)",
		},
		{
			name:    "link with a just anchor href",
			in:      "<a href=\"#anchor\">anchored link</a>",
			out:     "[anchored link](https://example.org/post/1#anchor)",
			baseURL: "https://example.org/post/1",
		},
		{
			name: "horizontal line",
			in:   "</hr>",
			out:  "",
		},
		{
			name: "tt to inline source code",
			in:   "<p>Just a <tt>inline source code</tt>.",
			out:  "Just a `inline source code`.",
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			a := assert.New(t)

			markdown, err := FromHTML(tt.in, tt.baseURL)

			a.NoError(err)
			a.Equal(tt.out, markdown)
		})
	}
}
