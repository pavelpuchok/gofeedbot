package format

import (
	"gofeedbot/internal/app/feed"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_formatFeedEntry(t *testing.T) {
	tests := []struct {
		name   string
		update *feed.Entry
		want   string
	}{
		{
			name: "No markup text",
			update: &feed.Entry{
				Title:       "Title",
				Description: "Description",
				URL:         "http://example.org",
			},
			want: "[Title](http://example.org)\nDescription",
		},
		{
			name: "Paragraph",
			update: &feed.Entry{
				Title:       "Title",
				Description: "<p><span>simple text</span> <strong>bold</strong></p>",
				URL:         "http://example.org",
			},
			want: "[Title](http://example.org)\nsimple text *bold*",
		},
		{
			name: "Local anchor to absolute URL",
			update: &feed.Entry{
				Title:       "Title",
				Description: "<a href=\"#anchor\">Link</a>",
				URL:         "http://example.org",
			},
			want: "[Title](http://example.org)\n[Link](http://example.org#anchor)",
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			a := assert.New(t)

			a.Equal(tt.want, FeedEntry(tt.update))
		})
	}
}
