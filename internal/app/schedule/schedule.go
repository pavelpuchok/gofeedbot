package schedule

import "time"

type Job func()

type Scheduler interface {
	Start()
	Stop()
}

type TickScheduler struct {
	job      Job
	done     chan interface{}
	duration time.Duration
}

func NewTickScheduler(duration time.Duration, job Job) *TickScheduler {
	return &TickScheduler{
		job:      job,
		duration: duration,
	}
}

func (s *TickScheduler) Start() {
	s.done = make(chan interface{}, 0)
	ticker := time.NewTicker(s.duration)

	go func() {
		for {
			select {
			case <-s.done:
				ticker.Stop()
				return
			case <-ticker.C:
				go s.runJob()
			}
		}
	}()
}

func (s *TickScheduler) runJob() {
	s.job()
}

func (s *TickScheduler) Stop() {
	close(s.done)
}
