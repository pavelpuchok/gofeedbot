#!/usr/bin/env bash

# Read all environment variables with FEED_BOT_ prefix
# and combine them to the follow JSON format (escaped to use in CLI):
# { "app_environment_variables":[ { "name": "var_name", "value": "var_value" } ] }
ENV_VARS=$(env | grep -E ^FEED_BOT_ | tr '\n' ' ' | jq -R 'rtrimstr(" ")|split(" ")|map(split("=")|{name:.[0],value:.[1]})|{app_environment_variables:.}')
BINARY="$(pwd)/gofeedbot"

cd deployments/ansible

echo $ENV_VARS > environment

ansible-playbook -i hosts \
    -e app_binary_file_src=${BINARY} \
    -e app_db_file_path=${FEED_BOT_DB_DATA_SOURCE} \
    --extra-vars=@./environment \
    site.yml

