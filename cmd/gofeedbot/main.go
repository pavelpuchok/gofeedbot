package main

import (
	"gofeedbot/internal/app"
	"gofeedbot/internal/app/log"
)

func main() {
	logger := log.New(log.DebugLevel)
	app, err := app.NewFromEnvVars(logger)

	if err != nil {
		logger.Fatal(log.Fields{
			"location": "main.main",
			"error":    err,
		}, "app initialization failed")
	}

	app.Run()
}
