BINARY=gofeedbot
GOLANGCILINT=${GOPATH}/bin/golangci-lint

.PHONY: test vet build fmt-check deploy lint

build:
	GOOS=linux GOARCH=amd64 go build -o $(BINARY) ./cmd/gofeedbot

clean:
	rm -f $(BINARY)

test:
	@go test -race -timeout 300ms ./...

lint:
	$(GOLANGCILINT) run

deploy:
	@scripts/deploy.sh
