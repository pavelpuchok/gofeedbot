module gofeedbot

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/Skarlso/html-to-markdown v0.0.0-20191210071215-2cf06e949e49
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/google/wire v0.4.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/mmcdole/gofeed v1.0.0-beta2
	github.com/rubenv/sql-migrate v0.0.0-20200402132117-435005d389bc
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.5.1
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200408040146-ea54a3c99b9b // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

replace github.com/mmcdole/gofeed => github.com/mmcdole/gofeed v1.0.0-beta2.0.20200307020205-1ed4d84b36ec
