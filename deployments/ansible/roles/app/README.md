app
=========

Intalling app binaries on instance.

Role Variables
--------------

* `app_binary_file_src` (required) --- a path to the app binary file on local host
* `app_binary_file_dest` --- a path to dir at target host where to place app binary
* `app_user_name` --- user name
* `app_user_group` --- user group
* `app_name` --- app name used as binary and service name
* `app_environment_variables` (required) --- list of objects describing env variables. Ex: `{ name: "TG_BOT_TOKEN", value: "TOKEN_VALUE" }`
* `app_db_file_path` (required) --- a path to sqlite db file

Example Playbook
----------------

```
- hosts: servers
  roles:
     - role: app
       vars:
         app_binary_file_src: "/build/myapp"
         app_environment_variables:
          - name: VAR_NAME
            value: VAR_VALUE
        app_db_file_path: /var/local/myapp/myapp.db
```

License
-------

MIT
