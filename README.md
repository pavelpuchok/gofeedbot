# Go to read the feed

RSS/Atom telegram bot reader.

## Required environment variables for run app
* `FEED_BOT_TOKEN` --- telegram bot token
* `FEED_BOT_DB_DATA_SOURCE` --- SQLite DB data source string
* `FEED_BOT_UPDATE_FREQUENCY` --- Update feed duration. 10s, 1m, 1h

## Required environment variables for deployment
* `ANSIBLE_REMOTE_USER` --- name of user to login to target host
* `ANSIBLE_PRIVATE_KEY_FILE` --- path at local host to private ssh key
* `ANSIBLE_PYTHON_INTERPRETER` --- path to python bin at target host
* `APP_HOST` --- target host name or address

## Requeired variables for deployment
* `app_binary_file_src` --- path at host system to bot binary file
* `app_environment_variables` --- a list of environment variables. See [deploy.sh](scripts/deploy.sh) and [app-environment.j2](deployments/ansible/roles/app/templates/app-environment.j2) for more information
* `app_db_file_path` --- a path to SQLite DB file
